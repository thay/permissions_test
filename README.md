# Repository to test file permissions when checking out git repos

Contains 2 files, one which *should* have +x, and one without +x.

Instructions:
* git clone ...
* cd permissions_test
* git status

If there is no difference, then the filesystem is behaving nicely
